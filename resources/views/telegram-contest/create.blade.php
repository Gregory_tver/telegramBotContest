<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Создание конкурса для телеграма') }}
        </h2>
    </x-slot>

    <div class="container mt-5 bg-white p-5">
        <div class="row">
        <div class="col-sm-12 offset-md-3 col-md-6">
            <form method="POST" action="{{ route('telegram-contest.store') }}" class="needs-validation" novalidate>
                @csrf
                @method('POST')
                @error('exception')
                    <div class="alert alert-danger d-flex align-items-center" role="alert">
                        <div>{{$message}}</div>
                    </div>
                @enderror
                <div class="mb-3">
                    <label for="name" class="form-label">Название</label>
                    <input id="name" name="name" type="text" value="{{old('name')}}" class="form-control" >
                    @error('name')
                        <span class="text-danger" >{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="status_id" class="form-label">Статус конкурса</label>
                    <select class="form-select" name="status_id" required>
                        @foreach($contestStatuses as $name => $id)
                            <option
                                @if (old('name') == $name)
                                    selected
                                @elseif ($loop->first)
                                    selected
                                @endif
                                value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="count_winners" class="form-label">Количество победителей</label>
                    <input id="count_winners" name="count_winners"  type="number" class="form-control" value="{{old('count_winners')}}">
                    @error('count_winners')
                        <span class="text-danger" >{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="date_end" class="form-label">Время запуска скрипта поиска победителей.</label>
                    <input id="date_end" name="date_end" type="datetime-local"  class="form-control" value="{{old('date_end')}}">
                    @error('date_end')
                        <span class="text-danger" >{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Описание</label>
                    <textarea class="form-control" name="description" id="description" cols="30" rows="4">{{old('description')}}</textarea>

                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button class="btn btn-success" type="submit">Создать</button>
                </div>
            </form>
        </div>
        </div>
    </div>
</x-app-layout>
