<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $contest->name }}
        </h2>
    </x-slot>

    <div class="container mt-5 bg-white p-5">
        <div class="col-12">
                <a href="{{route('telegram-contest.edit', $contest->id)}}" class="btn btn-success mt-4">Редактировать</a>
                <form class="d-inline-block" method="POST" action="{{route('telegram-contest.destroy', $contest->id)}}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger mt-4"
                            onclick="return confirm('Подтверждаете удаление ?')"
                            type="submit">Удалить
                    </button>
                </form>

        </div>

        <div class="col-12 mt-5">
            <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home"
                            type="button" role="tab" aria-controls="home" aria-selected="true">Общие данные
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="participants-tab" data-bs-toggle="tab" data-bs-target="#participants"
                            type="button" role="tab" aria-controls="participants" aria-selected="false">Участники
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="winners-tab" data-bs-toggle="tab" data-bs-target="#winners"
                            type="button" role="tab" aria-controls="winners" aria-selected="false">Победители
                    </button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="col-6 offset-3">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th scope="row">Название</th>
                                <td>{{$contest->name}}</td>
                            </tr>

                            <tr>
                                <th scope="row">Количество победителей</th>
                                <td>{{$contest->count_winners}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Статус</th>
                                <td>{{$contest->status->name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Дата подведения итогов</th>
                                <td>{{$contest->date_end}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Описание</th>
                                <td>{{$contest->description}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="participants" role="tabpanel" aria-labelledby="participants-tab">
                    @if ( count($contest->participants) > 0)
                        <table class="table mt-3 table-sm">
                            <thead>
                            <tr class="table-light">
                                <th scope="col">#</th>
                                <th scope="col">ID</th>
                                <th scope="col">ID в телеграме</th>
                                <th scope="col">Ник</th>
                                <th scope="col">Имя</th>
                                <th scope="col">Фамилия</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($contest->participants as $participant)
                                <tr>
                                    <th scope="row">{{$loop->index}}</th>
                                    <td>{{$participant->id}}</td>
                                    <td>{{$participant->telegram_user->telegram_id}}</td>
                                    <td>{{$participant->telegram_user->username}}</td>
                                    <td>{{$participant->telegram_user->first_name}}</td>
                                    <td>{{$participant->telegram_user->last_name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Пока никто не зарегистрировался на конкурс!</h4>
                            <p>Еще нет участников, как только человек подтвердит свое участие, тут отобразится таблица с
                                участниками </p>
                            <hr>
                            <p class="mb-0">Как запустить конкурс можно прочитать <a href="#">тут</a></p>
                        </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="winners" role="tabpanel" aria-labelledby="winners-tab">
                    @if ( count($contest->winners) > 0)
                        <table class="table mt-3 table-sm">
                            <thead>
                            <tr class="table-success">
                                <th scope="col">#</th>
                                <th scope="col">ID</th>
                                <th scope="col">ID в телеграме</th>
                                <th scope="col">Ник</th>
                                <th scope="col">Имя</th>
                                <th scope="col">Фамилия</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($contest->winners as $winner)
                                <tr>
                                    <th scope="row">{{$loop->index}}</th>
                                    <td>{{$winner->id}}</td>
                                    <td>{{$winner->telegram_user->telegram_id}}</td>
                                    <td>{{$winner->telegram_user->username}}</td>
                                    <td>{{$winner->telegram_user->first_name}}</td>
                                    <td>{{$winner->telegram_user->last_name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            Нет победителей
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
