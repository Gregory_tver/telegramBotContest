<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Телеграм конкурсы') }}
        </h2>
    </x-slot>

    <div class="container mt-5 bg-white p-5">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Активные</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Завершенные</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <a href="{{route('telegram-contest.create')}}" class="btn btn-success mt-4">Добавить конкурс</a>

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table mt-3 table-hover">
                        <thead>
                        <tr class="table-light">
                            <th scope="col">#</th>
                            <th scope="col">Название</th>
                            <th scope="col">Описание</th>
                            <th scope="col">Статус</th>
                            <th scope="col">Кол-во участников</th>
                            <th scope="col">Кол-во победителей</th>
                            <th scope="col">Дата подведения итогов</th>
                            <th scope="col">Создан</th>
                            <th scope="col">Изменен</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($contests)
                            @foreach($contests as $contest)
                                @if($contest->status->id < \App\Containers\TelegramContest\Models\ContestStatus::ID_STATUS_DONE)
                                    <tr
                                        @if($contest->status_id === \App\Containers\TelegramContest\Models\ContestStatus::ID_STATUS_ACTIVE)
                                            class="table-success"
                                        @endif
                                        @if($contest->status_id === \App\Containers\TelegramContest\Models\ContestStatus::ID_STATUS_PENDING)
                                            class="table-warning"
                                        @endif
                                    >
                                        <th scope="row">{{$loop->index}}</th>
                                        <td><a href="{{route('telegram-contest.show', $contest->id)}}">{{$contest->name}}</a> </td>
                                        <td>{{$contest->description}}</td>
                                        <td>{{$contest->status->name}}</td>
                                        <td>{{$contest->participants->count()}}</td>
                                        <td>{{$contest->winners->count()}}</td>
                                        <td>{{$contest->date_end}}</td>
                                        <td>{{$contest->created_at}}</td>
                                        <td>{{$contest->updated_at}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table mt-3 table-hover">
                        <thead>
                        <tr class="table-light">
                            <th scope="col">#</th>
                            <th scope="col">Название</th>
                            <th scope="col">Описание</th>
                            <th scope="col">Статус</th>
                            <th scope="col">Кол-во участников</th>
                            <th scope="col">Кол-во победителей</th>
                            <th scope="col">Дата подведения итогов</th>
                            <th scope="col">Создан</th>
                            <th scope="col">Изменен</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($contests)
                            @foreach($contests as $contest)
                                @if($contest->status->id == \App\Containers\TelegramContest\Models\ContestStatus::ID_STATUS_DONE)
                                    <tr>
                                        <th scope="row">{{$loop->index}}</th>
                                        <td><a href="{{route('telegram-contest.show', $contest->id)}}">{{$contest->name}}</a></td>
                                        <td>{{$contest->description}}</td>
                                        <td>{{$contest->status->name}}</td>
                                        <td>{{$contest->participants->count()}}</td>
                                        <td>{{$contest->winners->count()}}</td>
                                        <td>{{$contest->date_end}}</td>
                                        <td>{{$contest->created_at}}</td>
                                        <td>{{$contest->updated_at}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
