

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Главная') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2>Добро пожаловать! </h2>
                    <p>Для того что-бы начать нужно:</p>
                    <ul>
                        <li>1. Перейти в раздел "Телеграм конкурсы"</li>
                        <li>2. Создать конкурс</li>
                        <li>3. Дождаться, когда появится победитель</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
