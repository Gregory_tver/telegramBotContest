<?php

namespace App\Containers\TelegramContest\DTO;

abstract class TransformerToTelegramUserDTO
{
    static public function transformFromArray(array $data): TelegramUserDTO
    {
        $dto = new TelegramUserDTO();

        $dto->telegram_id = \Arr::get($data, 'id');
        $dto->username = \Arr::get($data,'username');
        $dto->first_name = \Arr::get($data,'first_name');
        $dto->last_name = \Arr::get($data, 'last_name');

        return $dto;
    }
}
