<?php

namespace App\Containers\TelegramContest\DTO;

class TelegramUserDTO
{
    public $telegram_id;
    public $username;
    public $first_name;
    public $last_name;
}
