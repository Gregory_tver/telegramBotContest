<?php

namespace App\Containers\TelegramContest\DTO;

abstract class TransformerToContestDTO
{
    static public function transformFromArray(array $data): ContestDTO
    {
        $dto = new ContestDTO();

        $dto->name = \Arr::get($data, 'name');
        $dto->description = \Arr::get($data, 'description');
        $dto->date_end = \Arr::get($data, 'date_end');
        $dto->status_id = \Arr::get($data, 'status_id');
        $dto->count_winners = \Arr::get($data, 'count_winners');

        return $dto;
    }
}
