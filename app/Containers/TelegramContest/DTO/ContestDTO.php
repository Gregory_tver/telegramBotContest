<?php

namespace App\Containers\TelegramContest\DTO;

class ContestDTO
{
    public $id;
    public $name;
    public $description;
    public $date_end;
    public $count_winners;
    public $status_id;
}
