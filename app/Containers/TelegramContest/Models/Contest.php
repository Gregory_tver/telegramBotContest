<?php

namespace App\Containers\TelegramContest\Models;


use Illuminate\Database\Eloquent\Model;


class Contest extends Model
{
    protected $table = 'contests';

   public function status()
   {
       return $this->belongsTo(ContestStatus::class);
   }

   public function winners()
   {
       return $this->hasMany(ContestWinner::class);
   }

    public function participants()
    {
        return $this->hasMany(ContestParticipant::class);
    }
}
