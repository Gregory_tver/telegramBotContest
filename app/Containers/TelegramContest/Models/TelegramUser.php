<?php

namespace App\Containers\TelegramContest\Models;


use Illuminate\Database\Eloquent\Model;


class TelegramUser extends Model
{
    protected $table = 'telegram_users';
}
