<?php

namespace App\Containers\TelegramContest\Models;


use Illuminate\Database\Eloquent\Model;


class ContestStatus extends Model
{
    protected $table = 'contest_statuses';

    public $timestamps = false;

    const ID_STATUS_PENDING = 10;
    const ID_STATUS_ACTIVE =  20;
    const ID_STATUS_DONE   =  100;

    const IDS_ALL_STATUSES = [
        self::ID_STATUS_PENDING,
        self::ID_STATUS_ACTIVE,
        self::ID_STATUS_DONE,
    ];

    const IDS_FOR_CREATING_STATUSES = [
        self::ID_STATUS_PENDING,
        self::ID_STATUS_ACTIVE,
    ];
}
