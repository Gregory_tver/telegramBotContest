<?php

namespace App\Containers\TelegramContest\Models;


use Illuminate\Database\Eloquent\Model;


class ContestWinner extends Model
{
    protected $table = 'contest_winners';

    public function telegram_user()
    {
        return $this->belongsTo(TelegramUser::class);
    }

    public function contest()
    {
        return $this->belongsTo(Contest::class);
    }
}
