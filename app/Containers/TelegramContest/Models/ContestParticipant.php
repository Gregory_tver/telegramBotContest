<?php

namespace App\Containers\TelegramContest\Models;


use Illuminate\Database\Eloquent\Model;


class ContestParticipant extends Model
{
    protected $table = 'contest_participants';

    public function telegram_user()
    {
        return $this->belongsTo(TelegramUser::class);
    }

    public function contest()
    {
        return $this->belongsTo(Contest::class);
    }
}
