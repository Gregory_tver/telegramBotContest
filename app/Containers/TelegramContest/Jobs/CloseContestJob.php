<?php

namespace App\Containers\TelegramContest\Jobs;

use App\Containers\TelegramContest\Actions\SetRandomWinnersForContestAction;
use App\Containers\TelegramContest\Models\Contest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CloseContestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contest;

    public function __construct(Contest $contest)
    {
        $this->contest = $contest;
    }

    public function handle()
    {
        app(SetRandomWinnersForContestAction::class)->run($this->contest);
    }
}
