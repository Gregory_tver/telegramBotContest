<?php

namespace App\Containers\TelegramContest\Http\Controllers;

use App\Containers\TelegramContest\Actions\CreateNewContestAction;
use App\Containers\TelegramContest\Actions\DeleteContestByIdAction;
use App\Containers\TelegramContest\Actions\FindContestsAction;
use App\Containers\TelegramContest\Actions\FindContestStatusesForSelectAction;
use App\Containers\TelegramContest\Actions\UpdateContestAction;
use App\Containers\TelegramContest\DTO\TransformerToContestDTO;
use App\Containers\TelegramContest\Exceptios\TelegramContestException;
use App\Containers\TelegramContest\Http\Requests\ContestCreateUpdateRequest;
use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Tasks\FindContestByIdWithFullDataTask;
use App\Http\Controllers\Controller;


class TelegramContestController extends Controller
{
    public function index()
    {
       $contests = app(FindContestsAction::class)->run();
       return view('telegram-contest.index', compact('contests'));
    }

    public function create()
    {
        $contestStatuses = app(FindContestStatusesForSelectAction::class)->run();
        return view('telegram-contest.create', compact('contestStatuses'));
    }


    public function store(ContestCreateUpdateRequest $request)
    {
        $newContest = $request->all();
        $contestDTO = TransformerToContestDTO::transformFromArray($newContest);
        try {
           $contest = app(CreateNewContestAction::class)->run($contestDTO);
            return redirect()
                ->route('telegram-contest.show', $contest->id)
                ->with(['success' => 'Успешно сохранено']);
        } catch (TelegramContestException $e) {
            return back()
                ->withErrors(['exception' => $e->getMessage()])
                ->withInput();
        }
    }

    public function show($id)
    {
        $contest = app(FindContestByIdWithFullDataTask::class)->run($id);
        if (!$contest) {
            abort(404);
        }
        return view('telegram-contest.show', compact('contest'));
    }

    public function edit($id)
    {
        $contest = Contest::findOrFail($id);
        $contestStatuses = app(FindContestStatusesForSelectAction::class)->run();
        return view('telegram-contest.edit', compact('contest', 'contestStatuses'));
    }


    public function update(ContestCreateUpdateRequest $request, $id)
    {
        $contest = $request->all();
        $contestDTO = TransformerToContestDTO::transformFromArray($contest);
        $contestDTO->id = $id;

        try {
            $contest = app(UpdateContestAction::class)->run($contestDTO);
            return redirect()
                ->route('telegram-contest.show', $contest->id)
                ->with(['success' => 'Успешно сохранено']);
        } catch (TelegramContestException $e) {
            return back()
                ->withErrors(['exception' => $e->getMessage()])
                ->withInput();
        }
    }

    public function destroy($id)
    {
        app(DeleteContestByIdAction::class)->run($id);
        return redirect()->route('telegram-contest.index');
    }
}
