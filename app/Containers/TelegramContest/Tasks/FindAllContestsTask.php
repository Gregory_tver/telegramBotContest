<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\Contest;


class FindAllContestsTask
{
    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function run()
    {
        $contests = Contest::query()
            ->with([
                'winners',
                'participants',
                'status',
            ])
            ->get();

        return $contests;
    }
}
