<?php

namespace App\Containers\TelegramContest\Tasks;



use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestStatus;

class FindActiveContestTask
{
    public function run()
    {
        $contest = Contest::query()
        ->where('status_id', ContestStatus::ID_STATUS_ACTIVE)
        ->first();

        return $contest;
    }
}
