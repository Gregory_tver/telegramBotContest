<?php

namespace App\Containers\TelegramContest\Tasks;



use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestParticipant;
use App\Containers\TelegramContest\Models\TelegramUser;

class AddingTelegramUserToContestTask
{
    public function run(TelegramUser $telegramUser, Contest $contest): ContestParticipant
    {
        /**
         * @var ContestParticipant $contestParticipant
         */
        $contestParticipant = ContestParticipant::query()
            ->where('telegram_user_id', $telegramUser->id)
            ->where('contest_id', $contest->id)
            ->first();

        if ($contestParticipant) {
            return $contestParticipant;
        }

        $contestParticipant = new ContestParticipant();
        $contestParticipant->contest_id = $contest->id;
        $contestParticipant->telegram_user_id = $telegramUser->id;

        try {
            $contestParticipant->save();
        } catch (\Exception $e) {
            throw new \Exception('Ошибка присоединения к розыгрышу');
        }

        return $contestParticipant;
    }
}
