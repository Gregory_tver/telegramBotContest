<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\TelegramUser;
use App\Containers\TelegramContest\DTO\TelegramUserDTO;

class FindOrCreateTelegramUserTask
{
    public function run(TelegramUserDTO $telegramUserDTO): TelegramUser
    {
        $telegramUser = TelegramUser::query()
            ->where('telegram_id', $telegramUserDTO->telegram_id)
            ->first();

        if ($telegramUser) {
            /**  @var TelegramUser $telegramUser */
            return $telegramUser;
        }

        $telegramUser = new TelegramUser();
        $telegramUser->telegram_id = $telegramUserDTO->telegram_id;
        $telegramUser->username = $telegramUserDTO->username;
        $telegramUser->first_name = $telegramUserDTO->first_name;
        $telegramUser->last_name = $telegramUserDTO->last_name;

        try {
            $telegramUser->save();
        } catch (\Exception $e) {
            throw new \Exception('Ошибка сохранения пользователя телеграма');
        }

        return $telegramUser;
    }
}
