<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestParticipant;
use App\Containers\TelegramContest\Models\ContestWinner;

class SetRandomWinnersForContestTask
{
    public function run(Contest $contest)
    {
        $participantIds = $this->getParticipantIds($contest);
        if (empty($participantIds))
        {
            return;
        }

        $countWinners = $contest->count_winners;
        if (count($participantIds) < $contest->count_winners) {
            $countWinners = count($participantIds);
        }

        $winnersIds = \Arr::random($participantIds, $countWinners);

        foreach ($winnersIds as $winnersId) {
            $winner = new ContestWinner();
            $winner->contest_id = $contest->id;
            $winner->telegram_user_id = $winnersId;
            $winner->save();
        }

    }

    private function getParticipantIds(Contest $contest): array
    {
        return ContestParticipant::query()
            ->where('contest_id', $contest->id)
            ->pluck('telegram_user_id')
            ->toArray();
    }

}
