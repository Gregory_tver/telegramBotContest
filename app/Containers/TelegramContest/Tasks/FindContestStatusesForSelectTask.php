<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\ContestStatus;


class FindContestStatusesForSelectTask
{
    public function run(): array
    {
        $contestStatuses = ContestStatus::query()
            ->whereIn('id', ContestStatus::IDS_FOR_CREATING_STATUSES)
            ->pluck('id', 'name')
            ->toArray();
        return $contestStatuses;
    }
}
