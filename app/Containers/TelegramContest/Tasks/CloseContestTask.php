<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestStatus;

class CloseContestTask
{
    public function run(Contest $contest)
    {
        $contest->status_id = ContestStatus::ID_STATUS_DONE;
        $contest->save();
    }
}
