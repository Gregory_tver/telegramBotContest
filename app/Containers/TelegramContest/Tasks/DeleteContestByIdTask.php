<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\Contest;

class DeleteContestByIdTask
{
    public function run(int $id)
    {
        try {
            Contest::query()
                ->where('id', $id)
                ->delete();
        } catch (\Exception $e) {
            throw new \Exception('Ошибка удаления');
        }
    }
}
