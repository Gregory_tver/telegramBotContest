<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\Models\Contest;


class FindContestByIdWithFullDataTask
{

    public function run(int $id)
    {
        $contest = Contest::query()
            ->with([
                'status',
                'winners.telegram_user',
                'participants.telegram_user',
            ])
            ->find($id);

        return $contest;
    }
}
