<?php

namespace App\Containers\TelegramContest\Tasks;

use App\Containers\TelegramContest\DTO\ContestDTO;
use App\Containers\TelegramContest\Exceptios\TelegramContestException;
use App\Containers\TelegramContest\Jobs\CloseContestJob;
use App\Containers\TelegramContest\Models\Contest;
use DateTime;

class SaveContestTask
{
    public function run(ContestDTO $contestDTO, Contest $contest): Contest
    {
        $contest->name =  $contestDTO->name;
        $contest->status_id = $contestDTO->status_id;
        $contest->count_winners = $contestDTO->count_winners;
        $contest->date_end = $contestDTO->date_end;
        $contest->description = $contestDTO->description;
        try {
            $contest->save();
        } catch (\Exception $e) {
            throw new TelegramContestException('Ошибка Сохранения');
        }

        CloseContestJob::dispatch($contest)->delay(new DateTime($contest->date_end));

        return $contest;
    }
}
