<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Tasks\FindContestStatusesForSelectTask;


class FindContestStatusesForSelectAction
{
    public function run()
    {
        $contestStatuses = app(FindContestStatusesForSelectTask::class)->run();
        return $contestStatuses;
    }

}
