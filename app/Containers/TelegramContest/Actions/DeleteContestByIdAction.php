<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Tasks\DeleteContestByIdTask;


class DeleteContestByIdAction
{
    public function run(int $id)
    {
        app(DeleteContestByIdTask::class)->run($id);
    }
}
