<?php

namespace App\Containers\TelegramContest\Actions;


use App\Containers\TelegramContest\DTO\ContestDTO;
use App\Containers\TelegramContest\Exceptios\TelegramContestException;
use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestStatus;
use App\Containers\TelegramContest\Tasks\FindActiveContestTask;
use App\Containers\TelegramContest\Tasks\SaveContestTask;

class UpdateContestAction
{
    public function run(ContestDTO $contestDTO)
    {
        $activeContest = app(FindActiveContestTask::class)->run();
        if ($activeContest && $activeContest->id == ContestStatus::ID_STATUS_ACTIVE && $activeContest->id !== $contestDTO->id) {
            throw new TelegramContestException('В системе может быть только 1 активный розыгрыш');
        }

        $contest = Contest::find($contestDTO->id);
        return app(SaveContestTask::class)->run($contestDTO, $contest);
    }

}
