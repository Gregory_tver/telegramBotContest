<?php

namespace App\Containers\TelegramContest\Actions;


use App\Containers\TelegramContest\DTO\ContestDTO;
use App\Containers\TelegramContest\Exceptios\TelegramContestException;
use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestStatus;
use App\Containers\TelegramContest\Tasks\FindActiveContestTask;
use App\Containers\TelegramContest\Tasks\SaveContestTask;

class CreateNewContestAction
{
    public function run(ContestDTO $contestDTO): Contest
    {
        $activeContest = app(FindActiveContestTask::class)->run();

        if ($contestDTO->status_id == ContestStatus::ID_STATUS_ACTIVE && $activeContest) {
            throw new TelegramContestException('В системе может быть только 1 активный розыгрыш');
        }

        return app(SaveContestTask::class)->run($contestDTO, new Contest());
    }
}
