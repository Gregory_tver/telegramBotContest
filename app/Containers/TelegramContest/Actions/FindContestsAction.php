<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Tasks\FindAllContestsTask;


class FindContestsAction
{
    public function run()
    {
        $contests = app(FindAllContestsTask::class)->run();

        return $contests;
    }

}
