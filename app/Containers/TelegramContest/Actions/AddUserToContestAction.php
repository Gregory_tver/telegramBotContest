<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Models\ContestParticipant;
use App\Containers\TelegramContest\DTO\TelegramUserDTO;
use App\Containers\TelegramContest\Tasks\AddingTelegramUserToContestTask;
use App\Containers\TelegramContest\Tasks\FindActiveContestTask;
use App\Containers\TelegramContest\Tasks\FindOrCreateTelegramUserTask;

class AddUserToContestAction
{
    public function run(TelegramUserDTO $telegramUserDTO): ContestParticipant
    {
        $telegramUser       = app(FindOrCreateTelegramUserTask::class)->run($telegramUserDTO);
        $activeContest      = app(FindActiveContestTask::class)->run();
        $contestParticipant = app(AddingTelegramUserToContestTask::class)->run($telegramUser, $activeContest);

        return $contestParticipant;
    }

}
