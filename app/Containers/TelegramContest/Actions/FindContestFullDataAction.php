<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Tasks\FindAllContestsTask;
use App\Containers\TelegramContest\Tasks\FindContestByIdWithFullDataTask;


class FindContestFullDataAction
{
    public function run(int $id)
    {
        $contest = app(FindContestByIdWithFullDataTask::class)->run($id);
        return $contest;
    }

}
