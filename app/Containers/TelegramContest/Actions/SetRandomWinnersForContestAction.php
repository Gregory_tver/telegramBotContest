<?php

namespace App\Containers\TelegramContest\Actions;

use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Tasks\CloseContestTask;
use App\Containers\TelegramContest\Tasks\SetRandomWinnersForContestTask;

class SetRandomWinnersForContestAction
{
    public function run(Contest $contest)
    {
        app(SetRandomWinnersForContestTask::class)->run($contest);
        app(CloseContestTask::class)->run($contest);
    }
}
