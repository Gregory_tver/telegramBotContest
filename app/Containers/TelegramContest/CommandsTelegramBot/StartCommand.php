<?php

namespace App\Containers\TelegramContest\CommandsTelegramBot;

use App\Containers\TelegramContest\Models\Contest;
use App\Containers\TelegramContest\Models\ContestStatus;
use Telegram\Bot\Commands\Command;


class StartCommand extends Command
{
    protected $name = "start";

    protected $description = "Стартовая команда бота.";

    public function handle()
    {

        $contest = Contest::query()
            ->where('status_id', ContestStatus::ID_STATUS_ACTIVE)
            ->first();

        $helloText = 'Добро пожаловать в бот конкурсов!';
        $this->replyWithMessage(['text' => $helloText]);

        if ($contest) {
            $textStartContest = "Конкурс - '{$contest->name}'
Описание - '{$contest->description}'
Дата розыгрыша - '{$contest->date_end}'
Введите команду  /go чтобы участвовать!
";
        } else {
            $textStartContest = 'Пока нет активных конкурсов';
        }

        $this->replyWithMessage(['text' => $textStartContest]);
    }
}
