<?php

namespace App\Containers\TelegramContest\CommandsTelegramBot;

use App\Containers\TelegramContest\Actions\AddUserToContestAction;
use App\Containers\TelegramContest\DTO\TransformerToTelegramUserDTO;
use Telegram\Bot\Commands\Command;


class GoCommand extends Command
{
    protected $name = "go";

    protected $description = "Присоединиться к конкурсу";

    public function handle()
    {
        $dataUser = \Telegram::getWebhookUpdates()['message']['from'];
        $telegramUserDTO = TransformerToTelegramUserDTO::transformFromArray($dataUser);

        try {
            $contestParticipant = app(AddUserToContestAction::class)->run($telegramUserDTO);
            $message = "Отлично, вы участвуете в розыгрыше, ваш номер {$contestParticipant->id}";

        } catch (\Exception $exception) {
            $message = 'Произошла ошибка при добавлении в очередь';
        }


        $this->replyWithMessage(['text' => $message]);
    }
}
