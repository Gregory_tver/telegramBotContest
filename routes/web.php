<?php

use App\Containers\TelegramContest\Http\Controllers\TelegramContestController;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__ . '/auth.php';

Route::get('/', function () {return view('index');})
    ->middleware(['auth'])
    ->name('index');


Route::resource('telegram-contest', TelegramContestController::class)
    ->middleware(['auth'])
    ->names('telegram-contest');


Route::post(\Telegram::getAccessToken() . '/webhook', function () {
    //  $response = \Telegram::getMe();
    //  $botId = $response->getId();
    //  $firstName = $response->getFirstName();
    //  $username = $response->getUsername();
    //
    //https://api.telegram.org/bot1968880741:AAHioqDqcMPNEvUE_LQWzJWrZwxZgvujcX4/setwebhook?url=https://36ae-194-182-81-79.ngrok.io/1968880741:AAHioqDqcMPNEvUE_LQWzJWrZwxZgvujcX4/webhook

    $update = \Telegram::commandsHandler(true);
    return 'ok';
});
