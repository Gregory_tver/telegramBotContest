<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;


class CreateAdminUser extends Migration
{
    public function up()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@admin.ru';
        $user->password = Hash::make('telegram2021');
        $user->save();
    }

    public function down()
    {

    }
}
