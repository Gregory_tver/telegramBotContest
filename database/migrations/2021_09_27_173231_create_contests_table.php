<?php

use App\Containers\TelegramContest\Models\ContestStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestsTable extends Migration
{
    public function up()
    {
        Schema::create('contest_statuses', function (Blueprint $table) {
            $table->unsignedSmallInteger('id')->autoIncrement();
            $table->string('name', 255);
            $table->unsignedSmallInteger('sort')->default(100);
        });

        Schema::create('contests', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('description', 2000);
            $table->unsignedSmallInteger('status_id');
            $table->unsignedSmallInteger('count_winners')->default(1);
            $table->timestamp('date_end');
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('contest_statuses');
        });

        $statuses = [
            [
                'id' => ContestStatus::ID_STATUS_PENDING,
                'name' => 'В ожидании',
                'sort' => 100
            ],
            [
                'id' => ContestStatus::ID_STATUS_ACTIVE,
                'name' => 'Активный',
                'sort' => 200,
            ],
            [
                'id' => ContestStatus::ID_STATUS_DONE,
                'name' => 'Завершенный',
                'sort' => 1000,
            ],
        ];

        ContestStatus::query()->insert($statuses);
    }

    public function down()
    {
        Schema::dropIfExists('contests');
        Schema::dropIfExists('contest_statuses');
    }
}
